@extends('layouts.app')
  @section('title','Dahsboard')
  @section('page-title','Home')
  @section('content')
  <div class="row">
    <!-- left column -->
    <div class="col-md-8">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Kategori</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="{{ route('categori.store') }}" method="POST">
        @csrf
          <div class="box-body">
            <div class="form-group">
                @if($errors->has('nama_kategori'))
                    <div class="alert alert-danger">
                      <strong>{{ $errors->first('nama_kategori') }}</strong>
                    </div>
                @endif

              <label for="exampleInputEmail1">Nama Kategori</label>
              <input type="text" required class="form-control" name="nama_kategori" placeholder="Kategori"  value="{{ old('nama_kategori') }}">
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Tambah Kategori</button>
            <a href="{{ route('categori.index') }}" class="btn btn-danger">Kembali</a>
            <!-- <button type="submit" class="btn btn-danger">Kembali</button> -->

          </div>
        </form>
      </div>
    </div>
  </div>
      <!-- /.box -->

  @endsection